
//  ApplicationStates.swift
//  localizationTask
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.

import Foundation
import UIKit

enum Languages : Int{
    case arabic = 0
    case english = 1
    case hebrew = 2
    case hindi = 3
    
    
    var code : String {
        
        switch self
        {
           case .arabic:
            return "ar"
         
           case .english :
            return "en"
        
           default:
            return "hi"
        }
        
    }
    
    var orientation : NSTextAlignment {
        
        switch self {
        
        case .arabic ,
             .hebrew:
            
            return .left
        default:
            return .right

        }
        
    }
    
}


